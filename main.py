import ssl
import subprocess
import time
from bs4 import BeautifulSoup
import urllib.request

searx_instance="https://your-searx.instance"
llama_cpp_path = "/path/to/llama.cpp/main"
model_path = "/path/to/model.bin"


instructions = "Possible actions are Search[search term], Think[thought process], Skip. I need to decide."

initial_input = f"""### Human: When was Firefox created?
### Thinking: {instructions}?
### Action: Search[Firefox initial release]
### Result: November 9, 2004
### Assistant: Firefox was launched on November 9, 2004.
### Human: Thanks. How large is the eiffel tower?
### Thinking: {instructions}
### Action: Think[I know that]
### Assistant: The Eiffel Tower stands at 324 meters.
### Human: Where is Paris located?
### Thinking: {instructions}
### Action: Skip
### Assistant: The city of Paris is located in northern France."""
process = subprocess.Popen(
    [
        llama_cpp_path,
        '-m', model_path,
        '-i',
        '-ngl', '20', # remove if llama.cpp is not compiled with GPU acceleration
        '-c', '2048',
        '-r', "###",
        '-p', initial_input,
        '-t', '6',
        '-e',
        '--temp', '0.7',
        '--repeat-penalty', '1.176',
        '--top-k', '40',
        '--keep', '-1',
        '--multiline-input'
    ], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

output = ''
last_word = ''
last = "assistant" # remember what happend last
skip = initial_input.count("###")
search = False
line = 0
user_input = ""

while True:
    last_word += process.stdout.read(1).decode("utf-8", errors='ignore')

    # Differentiate based on Human/Assistant/Action/Search
    if "###" in last_word:
        print(last_word, end='')
        last_word = ''
        if skip:
            skip -= 1
            output = ''
        else:
            # Make prompt for human
            if last == "assistant":
                print(" Human: ", end='')
                user_input = input()
                input_string = "Human: " + user_input + f"\n### Thinking: {instructions}\n### Action:/\n"  # The AI may request a search
                process.stdin.write(input_string.encode("utf-8"))
                last = "action"
                skip = 2
            elif last == "action":
                if "Search" in output:
                    # get results
                    search_string = output[22:-2]
                    url = f"{searx_instance}/search?q={'%20'.join(x for x in search_string.split(' '))}&language=en&time_range=&safesearch=0&categories=general"
                    gcontext = ssl.SSLContext()
                    open_page = urllib.request.urlopen(url, context=gcontext).read()
                    soup = BeautifulSoup(open_page, "html.parser")
                    results = soup.find_all("p", {"class": "content"})
                    results_text = "".join(x.getText() for x in results[:num_results])

                    # Provide input
                    result_string = " SearchResult: " + results_text + f"\n### Thinking: Now I can reply to the input: \"{user_input}\". Or do I need another action? {instructions}\n### Action:/\n"  # AI
                    process.stdin.write(result_string.encode("utf-8"))
                    last = "action"
                    skip = 2
                elif "Skip" in output or "Think" in output:
                    # Skip action
                    process.stdin.write(" Assistant:/\n".encode("utf-8"))
                    last = "assistant"
                else:
                    # repeat instructions
                    process.stdin.write(f" Thinking: I replied with an incorrect action. {instructions}\nI should skip if I don't need an action.\n### Action:/\n".encode("utf-8"))
                    last = "action"
                    skip = 1
            output = ''
            process.stdin.flush()
    # Print output, so that the user can see what is happening
    elif last_word:
        if last_word[-1] == '\n':
            output += last_word
            print(last_word)
            last_word = ''
            line = 0
        elif last_word[-1] == ' ':
            output += last_word
            line += 1
            print(last_word, end='')
            last_word = ''
        if line > 20:
            print("")
            line = 0
    time.sleep(0.001)
